FROM openjdk:17-alpine

WORKDIR /app

COPY target/cake-manager*.jar /app/cake-manager.jar

EXPOSE 8080

CMD ["java", "-jar", "cake-manager.jar"]
