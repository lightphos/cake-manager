package com.waracle.cakemgr.service;

import com.waracle.cakemgr.entity.CakeEntity;
import com.waracle.cakemgr.entity.CakeRepository;
import com.waracle.cakemgr.exception.CakeException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.Fail.fail;

@SpringBootTest
@ActiveProfiles("test")
class CakeServiceIntegrationTest {

  @Autowired
  private CakeRepository cakeRepository;

  @Autowired
  private CakeService cakeService;

  @Test
  void downloadCakes_HasLemonCake() {
    cakeRepository.deleteAll();
    List<CakeEntity> cakeList = cakeService.downloadCakes();
    assertThat(cakeList).isNotNull();
    assertThat(cakeList.toString()).contains("Lemon cheesecake");
  }

  @Test
  void duplicate_ReturnsError() {
    cakeRepository.deleteAll();
    List<CakeEntity> cakeList = cakeService.downloadCakes();
    assertThat(cakeList).isNotNull();

    CakeEntity cakeEntity = cakeList.get(0);
    cakeEntity.setId(0L);
    try {
      cakeService.saveCake(cakeEntity);
      fail("Expect exception");
    } catch (CakeException e) {
      assertThat(e).isInstanceOf(CakeException.class);
    }
  }
}