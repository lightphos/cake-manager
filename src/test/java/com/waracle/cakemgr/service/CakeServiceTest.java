package com.waracle.cakemgr.service;

import com.waracle.cakemgr.entity.CakeEntity;
import com.waracle.cakemgr.entity.CakeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CakeServiceTest {

  @Mock
  private CakeRepository cakeRepository;

  @InjectMocks
  private CakeService cakeService;

  @Test
  void getCake_ReturnsCakeById() {
    long cakeId = 1L;
    CakeEntity cake = new CakeEntity(cakeId, "Chocolate Cake", "Chocolate", "c.com");
    when(cakeRepository.findById(cakeId)).thenReturn(Optional.of(cake));

    Optional<CakeEntity> result = cakeService.getCake(cakeId);

    assertThat(result.isPresent()).isTrue();
    assertThat(result.get()).isEqualTo(cake);
  }

  @Test
  void saveCake_ReturnsSavedCake() {
    CakeEntity newCake = new CakeEntity("Strawberry Cake", "Strawberry", "s.com");
    when(cakeRepository.save(newCake)).thenReturn(newCake);

    CakeEntity savedCake = cakeService.saveCake(newCake);

    assertThat(savedCake).isEqualTo(newCake);
  }

  @Test
  void updateCake_ReturnsUpdatedCake() {
    long cakeId = 1L;
    CakeEntity existingCake = new CakeEntity(cakeId, "Vanilla Cake", "Vanilla", "v.com");
    CakeEntity updatedCake = new CakeEntity(cakeId, "Updated Vanilla Cake", "Vanilla", "v.com");

    when(cakeRepository.findById(cakeId)).thenReturn(Optional.of(existingCake));
    when(cakeRepository.save(updatedCake)).thenReturn(updatedCake);

    CakeEntity result = cakeService.updateCake(cakeId, updatedCake);

    assertThat(result).isEqualTo(updatedCake);
  }

  @Test
  void deleteCake_DeletesCakeById() {
    Long cakeId = 1L;

    doNothing().when(cakeRepository).deleteById(cakeId);

    cakeService.deleteCake(cakeId);

    verify(cakeRepository, times(1)).deleteById(cakeId);
  }
}
