package com.waracle.cakemgr.controller;

import com.waracle.cakemgr.entity.CakeEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CakeControllerIntegrationTest {

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;


  @Test
  void getCakes_GivesCakes() {
    ResponseEntity<List<CakeEntity>> response = getListResponseEntity();

    List<CakeEntity> cakes = response.getBody();
    assertThat(cakes).isNotNull();
    assertThat(cakes.size()).isGreaterThan(1);
  }

  @Test
  void postCake_UpdateCake_GivesExpectedList() {
    // Add a new cake
    CakeEntity newCake = new CakeEntity("New Velvet", "New Flavour", "cake-image.com");
    ResponseEntity<CakeEntity> addResponse = restTemplate.postForEntity(
        "http://localhost:" + port + "/cakes", newCake, CakeEntity.class);
    assertThat(addResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);

    CakeEntity newCakeEntity = addResponse.getBody();
    assertThat(newCakeEntity).isNotNull();
    assertThat(newCakeEntity.getId()).isEqualTo(6);

    // Check it has been added
    assertCakeList("New Velvet");

    // Update its description
    CakeEntity updateCake = new CakeEntity("New Velvet", "New Improved Sugar Free Flavour", "cake-image.com");
    HttpHeaders headers = new HttpHeaders();
    headers.setBasicAuth("admin", "admin"); // Set basic auth credentials
    ResponseEntity<CakeEntity> updateResponse = restTemplate
        .exchange("http://localhost:" + port + "/cakes/" + newCakeEntity.getId(), HttpMethod.PUT,
        new HttpEntity<>(updateCake, headers), CakeEntity.class);
    assertThat(updateResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

    // check its updated
    assertCakeList("New Improved Sugar Free Flavour");
  }

  @Test
  void duplicatePostTitle_GivesError() {
    // Add cake with duplicate title
    CakeEntity dupCake = new CakeEntity("Lemon cheesecake", "New Lemon Flavour", "cake-image.com");
    HttpHeaders headers = new HttpHeaders();
    headers.setBasicAuth("admin", "admin"); // Set basic auth credentials
    ResponseEntity<CakeEntity> response = restTemplate
        .exchange("http://localhost:" + port + "/cakes", HttpMethod.POST,
            new HttpEntity<>(dupCake, headers), CakeEntity.class);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
  }

  private ResponseEntity<List<CakeEntity>> getListResponseEntity() {
    ResponseEntity<List<CakeEntity>> response = restTemplate.exchange(
        "http://localhost:" + port + "/cakes", HttpMethod.GET,
        null, new ParameterizedTypeReference<>() {
        }
    );
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    return response;
  }

  private void assertCakeList(String expect) {
    ResponseEntity<List<CakeEntity>> response = getListResponseEntity();
    List<CakeEntity> cakes = response.getBody();
    assertThat(cakes).isNotNull();
    assertThat(cakes.toString()).contains(expect);
  }
}
