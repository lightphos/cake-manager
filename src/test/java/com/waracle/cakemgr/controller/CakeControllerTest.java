package com.waracle.cakemgr.controller;

import com.waracle.cakemgr.entity.CakeEntity;
import com.waracle.cakemgr.service.CakeService;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class CakeControllerTest {

  private final CakeService cakeService = mock(CakeService.class);
  private final CakeController cakeController = new CakeController(cakeService);

  @Test
  void listCakes_ReturnsListOfCakes() {
    when(cakeService.listCakes()).thenReturn(Collections.singletonList(
        new CakeEntity("Chocolate Cake", "Chocolate", "c.com")));

    assertThat(cakeController.listCakes()).hasSize(1);
  }

  @Test
  void getCake_ReturnsCakeById() {
    Long cakeId = 1L;
    when(cakeService.getCake(cakeId)).thenReturn(Optional.of(
        new CakeEntity("Vanilla Cake", "Vanilla", "v.com")));

    assertThat(cakeController.getCake(cakeId).isPresent()).isTrue();
  }

  @Test
  void createCake_ReturnsCreatedResponse() {
    CakeEntity newCake = new CakeEntity("Strawberry Cake", "Strawberry","s.com");
    when(cakeService.saveCake(newCake)).thenReturn(newCake);

    ResponseEntity<CakeEntity> response = cakeController.createCake(newCake);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    assertThat(response.getBody()).isEqualTo(newCake);
  }

  @Test
  void updateCake_ReturnsUpdatedResponse() {
    Long cakeId = 1L;
    CakeEntity updatedCake = new CakeEntity("Updated Chocolate Cake", "Chocolate", "c.com");
    when(cakeService.updateCake(cakeId, updatedCake)).thenReturn(updatedCake);

    ResponseEntity<CakeEntity> response = cakeController.updateCake(cakeId, updatedCake);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(updatedCake);
  }

  @Test
  void deleteCake_ReturnsNoContentResponse() {
    Long cakeId = 1L;

    ResponseEntity<Void> response = cakeController.deleteCake(cakeId);

    verify(cakeService, times(1)).deleteCake(cakeId);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }
}
