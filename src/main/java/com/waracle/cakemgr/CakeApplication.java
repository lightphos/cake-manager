package com.waracle.cakemgr;

import com.waracle.cakemgr.entity.CakeEntity;
import com.waracle.cakemgr.service.CakeService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.util.List;

/**
 * The Cake application.
 */
@Slf4j
@SpringBootApplication
@EnableCaching
@OpenAPIDefinition(info =
@Info(title = "Cake Manager API", version = "${springdoc.version}", description = "Documentation CakeManager API v1.0")
)
@SecurityRequirement(name = "basicAuth") // for springdoc endpoint
public class CakeApplication {

    @Autowired
    CakeService cakeService;
    public static void main(String[] args) {
        SpringApplication.run(CakeApplication.class, args);
    }

    @PostConstruct
    public void init() {
        List<CakeEntity> cakes = cakeService.downloadCakes();
        log.info("Cakes stored {}", cakes.size());
    }
}
