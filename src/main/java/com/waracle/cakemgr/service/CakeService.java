package com.waracle.cakemgr.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.entity.CakeEntity;
import com.waracle.cakemgr.entity.CakeRepository;
import com.waracle.cakemgr.exception.CakeException;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

/**
 * The Cake service.
 */
@Service
@Slf4j
public class CakeService {

    private static final String CAKE_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/" +
            "8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    private final CakeRepository cakeRepository;

    /**
     * Instantiates a new Cake service.
     *
     * @param cakeRepository the cake repository
     */
    @Autowired
    public CakeService(CakeRepository cakeRepository) {
        this.cakeRepository = cakeRepository;
    }

    /**
     * List cakes iterable.
     *
     * @return the iterable
     */
    public Iterable<CakeEntity> listCakes() {
        return cakeRepository.findAll();
    }

    /**
     * Gets cake.
     *
     * @param id the id
     * @return the cake
     */
    public Optional<CakeEntity> getCake(Long id) {
        return cakeRepository.findById(id);
    }

    /**
     * Save cake entity.
     *
     * @param cakeEntity the cake entity
     * @return the cake entity
     */
    public CakeEntity saveCake(CakeEntity cakeEntity) {
        try {
            return cakeRepository.save(cakeEntity);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CakeException(e.getLocalizedMessage());
        }
    }

    /**
     * Update cake cake entity.
     *
     * @param id         the id
     * @param cakeEntity the cake entity
     * @return the cake entity
     */
    public CakeEntity updateCake(Long id, CakeEntity cakeEntity) {
        CakeEntity existingCake = cakeRepository.findById(id)
          .orElseThrow(() -> new EntityNotFoundException("No cake found with id: " + id));

        long cid = existingCake.getId();
        cakeEntity.setId(cid);
        return cakeRepository.save(cakeEntity);
    }

    /**
     * Delete cake.
     *
     * @param id the id
     */
    public void deleteCake(Long id) {
        cakeRepository.deleteById(id);
    }

    /**
     * Download cakes list.
     *
     * @return the list
     */
    public List<CakeEntity> downloadCakes() {
        log.info("downloading cake json");
        try (InputStream inputStream = new URL(CAKE_URL).openStream()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                log.info("Got <{}>", line);
                sb.append(line);
            }

            ArrayList<CakeEntity> cakeEntities = new ArrayList<>(marshallToCake(sb));
            persistCakes(cakeEntities);
            return cakeEntities;
        } catch (Exception e) {
            throw new CakeException(e.getMessage());
        }
    }

    private void persistCakes(List<CakeEntity> cakes) {
        int c = 1;
        for (CakeEntity cake : cakes) {
            log.info("cake {} {}", c++, cake);
        }
        cakeRepository.saveAll(cakes);
    }

    private Set<CakeEntity> marshallToCake(StringBuilder sb) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
           // use set to dedup
           return new HashSet<>(Arrays.asList(objectMapper.readValue(sb.toString(), CakeEntity[].class)));
        } catch (JsonProcessingException e) {
            throw new CakeException(e.getMessage());
        }
    }

}
