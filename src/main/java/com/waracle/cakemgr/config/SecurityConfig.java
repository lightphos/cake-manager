package com.waracle.cakemgr.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Value("${app.username:auser}")
  private String defUsername;
  @Value("${app.userpassword:apass}")
  private String defUserPwd;

  @Value("${app.adminname:admin}")
  private String defAdminName;
  @Value("${app.adminpassword:admin}")
  private String defAdminPwd;


  @Bean
  public InMemoryUserDetailsManager userDetailsService(PasswordEncoder passwordEncoder) {
    // <TODO> change to use OIDC or OAUTH2 or LDAP etc.
    UserDetails user = User.withUsername(defUsername)
        .password(passwordEncoder.encode(defUserPwd))
        .roles("USER")
        .build();

    UserDetails admin = User.withUsername(defAdminName)
        .password(passwordEncoder.encode(defAdminPwd))
        .roles("ADMIN", "USER")
        .build();

    return new InMemoryUserDetailsManager(user, admin);
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

    http.authorizeHttpRequests(authorize -> authorize
            .requestMatchers("/actuator/*").permitAll()
            .requestMatchers("/cakes").permitAll() // let all see our wonderful cakes
            .requestMatchers(HttpMethod.GET, "/cakes/*").hasAnyRole("USER") // if you are choosy you need to have rights
            .requestMatchers(HttpMethod.POST, "/cakes").hasAnyRole("ADMIN")
            .requestMatchers(HttpMethod.PUT, "/cakes/*").hasAnyRole("ADMIN")
            .requestMatchers(HttpMethod.DELETE, "/cakes/*").hasAnyRole("ADMIN")
            .anyRequest().authenticated()
        )
        .csrf(AbstractHttpConfigurer::disable)
        .httpBasic(withDefaults());
    return http.build();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }
}
