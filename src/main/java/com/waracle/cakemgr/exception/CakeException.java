package com.waracle.cakemgr.exception;

public class CakeException extends RuntimeException {
    public CakeException(String message) {
        super(message);
    }
}
