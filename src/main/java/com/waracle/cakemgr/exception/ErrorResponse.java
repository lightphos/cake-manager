package com.waracle.cakemgr.exception;

public record ErrorResponse(String message) {
}
