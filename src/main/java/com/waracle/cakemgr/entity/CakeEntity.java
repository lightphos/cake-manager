package com.waracle.cakemgr.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * The entity Cake.
 */
@jakarta.persistence.Entity
@Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CakeEntity {

  public CakeEntity() {}

  public CakeEntity(long id, String title, String desc, String image) {
    this.id = id;
    this.title = title;
    this.desc = desc;
    this.image = image;
  }

  public CakeEntity(String title, String desc, String image) {
    this.title = title;
    this.desc = desc;
    this.image = image;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", unique = true, nullable = false)
  private Long id;

  @Column(name = "TITLE", unique = true, nullable = false, length = 100)
  private String title;

  @Column(name = "DESC", nullable = false, length = 100)
  private String desc;

  @Column(name = "IMAGE", nullable = false, length = 300)
  private String image;
}
