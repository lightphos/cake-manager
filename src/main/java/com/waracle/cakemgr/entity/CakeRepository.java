package com.waracle.cakemgr.entity;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * The interface Cake repository.
 */
@RepositoryRestResource(exported = false)
public interface CakeRepository extends CrudRepository<CakeEntity, Long> {
  @Override
  @CacheEvict(value = "cakeCache", allEntries = true)
  <S extends CakeEntity> S save(S entity);

  @Override
  @CacheEvict(value = "cakeCache", allEntries = true)
  <S extends CakeEntity> Iterable<S> saveAll(Iterable<S> entities);

  @Override
  @Cacheable("cakeCache")
  Iterable<CakeEntity> findAll();
}
