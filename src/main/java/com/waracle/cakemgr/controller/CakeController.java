package com.waracle.cakemgr.controller;

import com.waracle.cakemgr.entity.CakeEntity;
import com.waracle.cakemgr.service.CakeService;
import jakarta.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * The Cake rest controller.
 */
@RestController
@RequestMapping("/cakes")
public class CakeController {

    private final CakeService cakeService;

    /**
     * Instantiates a new Cake controller.
     *
     * @param cakeService the cake service
     */
    @Autowired
    public CakeController(CakeService cakeService) {
        this.cakeService = cakeService;
    }

    /**
     * List cakes list.
     *
     * @return the list as json
     */
    @GetMapping(produces = "application/json")
    public List<CakeEntity> listCakes() {
       return StreamSupport.stream(cakeService.listCakes().spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     * Gets cake.
     *
     * @param id the id
     * @return the cake
     */
    @GetMapping(value = "{id}", produces = "application/json")
    public Optional<CakeEntity> getCake(@PathVariable Long id) {
        return cakeService.getCake(id);
    }

    /**
     * Create cake response entity.
     *
     * @param cakeEntity the cake entity
     * @return the response entity
     */
    @PostMapping(produces = "application/json")
    public ResponseEntity<CakeEntity> createCake(@RequestBody CakeEntity cakeEntity) {
        CakeEntity savedCake = cakeService.saveCake(cakeEntity);
        return new ResponseEntity<>(savedCake, HttpStatus.CREATED);
    }

    /**
     * Update cake response entity.
     *
     * @param id         the id
     * @param cakeEntity the cake entity
     * @return the response entity
     */
    @PutMapping(value = "{id}")
    public ResponseEntity<CakeEntity> updateCake(@PathVariable Long id, @RequestBody CakeEntity cakeEntity) {
        CakeEntity updateCake = cakeService.updateCake(id, cakeEntity);
        return new ResponseEntity<>(updateCake, HttpStatus.OK);
    }


    /**
     * Delete cake response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(value = "{id}")
    public ResponseEntity<Void> deleteCake(@PathVariable Long id) {
        cakeService.deleteCake(id);
        return ResponseEntity.noContent().build();
    }

}
