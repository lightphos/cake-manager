Cake Manager Micro Service (fictitious)
=======================================

## Cakes

This is the cake manager application. Here you can see mouth watering delights.

## API Endpoints 

See also the section on swagger below for detailed api documentation and UI tooling:

* GET /cakes: lists the cakes currently in the system.
* GET /cakes/{id}: lists the cake with the given id.

* POST /cakes: adds a new cake.

* PUT /cakes/{id}: Updates an existing cake.

* DELETE /cakes/{id}: Deletes an existing cake.

## Product Backlog

> We feel like the software stack used by the original developer is quite outdated, 
it would be good to migrate the entire application to something more modern.

> Supplemental requirements:
>* Add some suitable tests (unit/integration...)
>* Add some Authentication / Authorisation to the API
>* Continuous Integration via any cloud CI system
>* Containerisation


The following backlog helps meet the above requirements to modernize the application and put in
place standard software engineering practices.

* [x] Use Java 17 which has LTS (or 21)
* [x] Migrate to spring-boot, spring-data-rest stack
  * Pros. Well known stack with DI, Testability, SoC etc
  * Cons. Complexity, hidden magic, control, performance
* [x] Restructure to standard directory demarcations
* [x] Use lombok to reduce boilerplate code 
* [x] Use caching to reduce load on database (nice to have)
* [x] Add new cake, update existing, delete existing
* [x] Add integration/unit tests
* [ ] Distinguish between integration and unit tests
* [x] Coverage test report
* [x] Add API doc (swagger), (nice to have)
* [ ] Generate API clients (future)
* [x] Add authn/z basic with dev users and roles (insecure)
* [ ] Add authn/z with OIDC etc (future)
* [x] Add containerization (eg. build-pack, Dockerfile)
* [ ] API versioning (/v1 etc) (future)
* [x] CI in gitlab 
* [ ] Add architecture docs with ascii docs (future)
* [ ] Security validation XSS, SQL injection etc (should have) (out of the box?)
* [x] Check CVE/FOSS security against OWASP list (should have)

## Development

The development uses the following technologies:
- Java 17, Springboot 3.2.3, hsqldb, Spring Data Rest, Springdoc (Swagger)
- Lombok (need to turn on annotation processing in the IDE) and install the lombok plugin.
- maven
- gitlab ci
- bld tool - not essential, download from here (https://gitlab.com/lightphos/bld/-/releases)

## Test

`bld test
or
mvn test`

## Coverage

Run
`bld cov` or

`mvn clean test`
`mvn jacoco:report`

Report is in `cake-manager/target/site/jacoco/index.html`

## Running the application
To run locally execute the following command:

`bld run` or
`mvn spring-boot:run`

and access the application using following URL:

http://localhost:8282/cakes

Two defaults users are built in for now to allow testing:
-  user/pass role USER
-  admin/admin role ADMIN

These can be overridden by the following environment variables:

- APP_USER_NAME, APP_USER_PASSWORD
- APP_ADMIN_NAME. APP_ADMIN_PASSWORD

### Swagger

The code provides a swagger endpoint.

http://localhost:8282/swagger-ui/index.html

> NOTE: this stack uses spring data rest which can provide CRUD endpoint apis 'out of the box',
but we have added our own custom endpoint 'cake-controller', and turned off the
swagger api for spring data rest.

OpenAPI docs:
http://localhost:8282/v3/api-docs

### JavaDocs 
Some java docs (not a lot).

`mvn javadoc:javadoc`

Results in:

`cake-manager/target/site/apidocs/index.html`

### Containerization

Uses docker.

> NOTE: need docker installed and running.
> On macos/windows: can use docker-desktop


`bld image` or
`mvn package -DskipTests && docker build -t cake-manager-app .`

`docker images | grep cake`

shows:
`cake-manager-app lastest`

run it up:

`docker run -it -p 8080:8282 -e "APP_ADMIN_NAME=sys" -e "APP_ADMIN_PASSWORD=sys" cake-manager-app`

and point the browser to:

http://localhost:8080/cakes

## Security Checking

`bld int
or
mvn verify`

This will check against the OWASP list. Running it the first time downloads the whole set 
so this will take some time initially (long time). 

Results in target:
`target/dependency-check-report.html`


## Continuous Integration

Uses gitlab in built CI pipeline. See the file gitlab-ci.yml.

Build creates docker images and publishes them to the inbuilt registery:

`https://gitlab.com/lightphos/cake-manager/container_registry`

### Tags
Deploys the final artifact (docker image) to an environment (TBD) when a tag is pushed.


## Change Log

| Date       | Description                        | 
|------------|------------------------------------|
| 26-02-2024 | fix cake endpoint                  |
| 27-02-2024 | task complete restructure/refactor |
